import cv2
import faceDetect as fd
import geometryAlgorithm as ga
import numpy as np
import time

def getExpandRect(points, img_shape):
    t = points[0][1]
    b = points[0][1]
    l = points[0][0]
    r = points[0][0]
    for p in points:
        if l > p[0]:
            l = p[0]
        if r < p[0]:
            r = p[0]
        if t > p[1]:
            t = p[1]
        if b < p[1]:
            b = p[1]
    w = r - l
    h = b - t
    nt = max(t - h // 10, 0)
    nb = min(b + h // 10, img_shape[0])
    nl = max(l - w // 10, 0)
    nr = min(r + w // 10, img_shape[1])
    return  nl, nt, nr, nb


def expandPoints(points, img_shape):
    nl, nt, nr, nb = getExpandRect(points, img_shape)
    return np.append(points, [[nl,nt],[nl,nb],[nr,nt],[nr,nb],[(nl+nr)//2,nt],[(nl+nr)//2,nb],[nr,(nt+nb)//2],[nl,(nt+nb)//2]],axis=0)

def to_int(number):
    return int(number.item())

# print("ThuyDD2")
# src_image = cv2.imread("faces/face8.jpg")
# dst_image = cv2.imread("faces/face7.jpg")

def transform(src_image, dst_image):
    src_shape = fd.landmarkDetect(src_image)
    ls, ts, rs, bs =  getExpandRect(src_shape, src_image.shape[:2])
    # ls, ts, rs, bs = to_int(ls), to_int(ts), to_int(rs), to_int(bs)

    ws = rs - ls
    hs = bs - ts
    # print(sr - sl, sb - st)
    src_shape = expandPoints(src_shape, src_image.shape[:2])
    src_triangles = ga.getDelaunayTriangles(src_shape)
    dst_shape = fd.landmarkDetect(dst_image)
    left, top, right, bot = getExpandRect(dst_shape, dst_image.shape[:2])
    # print(left, top, right, bot)
    width = right - left
    height = bot - top
    # print('height, width', width, height)
    dst_shape = expandPoints(dst_shape, dst_image.shape[:2])
    dst_triangles = ga.getDelaunayTriangles(dst_shape)

    # print(src_shape)
    # print(dst_shape)

    map_x = np.zeros((height, width), dtype=np.float32)
    map_y = np.zeros((height, width), dtype=np.float32)

    # time1 = time.time()
    for dst_tri in dst_triangles:
        da = dst_shape[dst_tri[0]]
        db = dst_shape[dst_tri[1]]
        dc = dst_shape[dst_tri[2]]
        sa = src_shape[dst_tri[0]]
        sb = src_shape[dst_tri[1]]
        sc = src_shape[dst_tri[2]]
        points = ga.getIntegralPoints(da,db,dc)
        for p in points:
            v = p[1] - top
            u = p[0] - left
            if u<0 or v<0 or u >= width or v>= height:
                continue
            a_area = ga.triangleArea(p, db, dc)
            b_area = ga.triangleArea(p, da, dc)
            c_area = ga.triangleArea(p, da, db)
            s_area = a_area + b_area + c_area
            map_x[v, u] = a_area * sa[0] / s_area + b_area * sb[0] / s_area + c_area * sc[0] / s_area
            map_y[v, u] = a_area * sa[1] / s_area + b_area * sb[1] / s_area + c_area * sc[1] / s_area

    # print("time:", time.time()-time1)

    out_image = cv2.remap(src_image, map_x, map_y, cv2.INTER_LINEAR)


    # for (x, y) in src_shape:
    #     cv2.circle(src_image, (x, y), 2, (0, 255, 0), -1)

    # for (x, y) in dst_shape:
    #     cv2.circle(dst_image, (x, y), 2, (0, 255, 0), -1)
    #     cv2.circle(out_image, (x-left, y-top), 2, (0, 255, 0), -1)

    # Resize

    # interpolation method
    if height > hs or width > ws: # shrinking image
        interp = cv2.INTER_AREA

    else: # stretching image
        interp = cv2.INTER_CUBIC
    out_image = cv2.resize(out_image, (ws, hs), interpolation=interp)
    # cv2.imwrite("src_image.jpg", src_image)
    # cv2.imwrite("dst_image.jpg", dst_image)
    # cv2.imwrite("out_image.jpg", out_image)
    reshaped_image = src_image.copy()
    reshaped_image[ts:bs,ls:rs] = out_image

    
    return reshaped_image

# cv2.imshow("src_image", src_image)
# cv2.imshow("dst_image", dst_image)
# cv2.imshow("out_image", out_image)
# cv2.imshow("reshaped_image", reshaped_image)
# cv2.imwrite("src_image.jpg", src_image)
# cv2.imwrite("dst_image.jpg", dst_image)
# cv2.imwrite("out_image.jpg", out_image)
# cv2.imwrite("reshaped_image.jpg", reshaped_image)

# cv2.waitKey(60000)
