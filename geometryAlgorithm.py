import scipy.spatial as ss

bigNumber = 10000000

def getDelaunayTriangles(points):
    d = ss.Delaunay(points)
    return d.simplices

def triangleArea(a, b, c):

    return abs((a[0] * (b[1] - c[1]) + b[0] * (c[1] - a[1]) + c[0] * (a[1] - b[1])) / 2.0)

def sameSide(p1,p2,a,b):
    vec_ab = (b[0]-a[0],b[1]-a[1])
    A = vec_ab[1]
    B = -vec_ab[0]
    C = vec_ab[0]*a[1]-vec_ab[1]*a[0]
    return (A*p1[0]+B*p1[1]+C) * (A*p2[0]+B*p2[1]+C) >= 0

def insideTriangle(p, a, b, c):
    return sameSide(p,a,b,c) and sameSide(p,b,c,a) and sameSide(p,c,a,b)

def getIntegralPoints(a, b, c):
    points = []
    ymin = int(min(a[1],b[1],c[1]))
    ymax = int(max(a[1],b[1],c[1]))+1
    for y in range(ymin, ymax + 1):
        xmin = bigNumber
        xmax = -bigNumber
        if a[1] == y:
            xmin = min(a[0], xmin)
            xmax = max(a[0], xmax)
        if b[1] == y:
            xmin = min(b[0],xmin)
            xmax = max(b[0], xmax)
        if c[1] == y:
            xmin = min(c[0],xmin)
            xmax = max(c[0], xmax)
        if min(a[1],b[1]) < y and max(a[1],b[1]) > y:
            xtemp = a[0]+ (y - a[1]) * (b[0] - a[0]) / (b[1] - a[1])
            xmin = min(xtemp, xmin)
            xmax = max(xtemp, xmax)
        if min(b[1],c[1]) < y and max(b[1],c[1]) > y:
            xtemp = b[0]+ (y - b[1]) * (c[0] - b[0]) / (c[1] - b[1])
            xmin = min(xtemp, xmin)
            xmax = max(xtemp, xmax)
        if min(a[1],c[1]) < y and max(a[1],c[1]) > y:
            xtemp = a[0]+ (y - a[1]) * (c[0] - a[0]) / (c[1] - a[1])
            xmin = min(xtemp, xmin)
            xmax = max(xtemp, xmax)
        xmin = int(xmin)
        xmax = int(xmax)+1
        if xmin == bigNumber or xmax == -bigNumber:
            continue
        for x in range(xmin, xmax + 1):
            if x == xmin or x == xmax:
                if insideTriangle([x,y], a, b, c):
                    points.append([x,y])
            else:
                points.append([x, y])
    return points
