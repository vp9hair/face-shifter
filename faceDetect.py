from imutils import face_utils
import dlib
import cv2

landmarkDataPath = "shape_predictor_68_face_landmarks.dat"
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor(landmarkDataPath)

def landmarkDetect(img):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # detect faces in the grayscale image
    rects = detector(gray, 0)
    if len(rects) < 1:
        return None
    shape = predictor(gray, rects[0])
    shape = face_utils.shape_to_np(shape)
    return shape
