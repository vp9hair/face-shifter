import sys
import torch
sys.path.append('./face_modules/')
import torchvision.transforms as transforms
import torch.nn.functional as F
from face_modules.model import Backbone, Arcface, MobileFaceNet, Am_softmax, l2_norm
from network.AEI_Net import *
from face_modules.mtcnn import *
import cv2
import PIL.Image as Image
import numpy as np
import configparser
from logging import warning
import dlib
from scipy.spatial import Delaunay
from image_transform import transform

def cv2_imshow(x):
    cv2.imshow(x)
    cv2.waitKey(0)

# def load_stage1():
detector = MTCNN()
device = torch.device('cuda')
G = AEI_Net(c_id=512)

# print(G.summary)
G.eval()
G.load_state_dict(torch.load('./saved_models/G_latest.pth', map_location=torch.device('cuda')))
G = G.cuda()

arcface = Backbone(50, 1, 'ir_se').to(device)
arcface.eval()
arcface.load_state_dict(torch.load('./face_modules/model_ir_se50.pth', map_location=device), strict=False)

test_transform = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
])

def crop_face(img_path):
    Xt_raw = cv2.imread(img_path)
    try:
        Xt, _ = detector.align(Image.fromarray(Xt_raw[:, :, ::-1]), crop_size=(256, 256), return_trans_inv=True)
        print(img_path, Xt, type(Xt))
        cv2.imwrite(img_path, cv2.cvtColor(np.asarray(Xt), cv2.COLOR_BGR2RGB))
        return Xt
    except Exception as e:
        warning(e)
        warning("!! Detect failed, check the image again!")

def get_points(image):
    predictor_model = 'shape_predictor_68_face_landmarks.dat'
    # Use dlib to get the face landmarks(68 points)
    face_detector = dlib.get_frontal_face_detector()
    face_pose_predictor = dlib.shape_predictor(predictor_model)
    try:
        detected_face = face_detector(image, 1)[0]
    except:
        print('No face detected in image {}'.format(image))
    pose_landmarks = face_pose_predictor(image, detected_face)
    points = []
    for p in pose_landmarks.parts():
        points.append([p.x, p.y])

    # Add 8 image frame coordinate points
    x = image.shape[1] - 1
    y = image.shape[0] - 1
    points.append([0, 0])
    points.append([x // 2, 0])
    points.append([x, 0])
    points.append([x, y // 2])
    points.append([x, y])
    points.append([x // 2, y])
    points.append([0, y])
    points.append([0, y // 2])
    # Add extra image frame coordinate points
    x0, y0 = points[0]
    x16, y16 = points[16]
    points.append([x0, 0])
    points.append([x0, y0//2])
    points.append([x16, 0])
    points.append([x16, y16//2])

    return np.array(points)

def get_triangles(points):
    return Delaunay(points).simplices

def affine_transform(input_image, input_triangle, output_triangle, size):
    warp_matrix = cv2.getAffineTransform(
        np.float32(input_triangle), np.float32(output_triangle))
    output_image = cv2.warpAffine(input_image, warp_matrix, (size[0], size[1]), None,
                                  flags=cv2.INTER_LINEAR, borderMode=cv2.BORDER_REFLECT_101)
    return output_image

def reshape(Xs, Xt):
    rows,cols, ch = Xs.shape
    points1 = get_points(Xs)
    points2 = get_points(Xt)
    pts1 = np.float32([points1[0],points1[16],points1[8]])
    pts2 = np.float32([points2[0],points2[16],points2[8]])

    M = cv2.getAffineTransform(pts1,pts2)

    dst = cv2.warpAffine(Xs,M,(cols,rows))
    # cv2.imwrite('test.jpg', dst)
    ## reshape target

    img1 = Xt
    img2 = dst
    points1 = get_points(img1)
    points2 = get_points(img2)
    # img_morphed = np.zeros(img1.shape, dtype=img1.dtype)
    img_morphed = np.zeros(img1.shape, dtype=img1.dtype)
    triangles1 = get_triangles(points1)
    triangles2 = get_triangles(points2)
    points = np.array(points1)
    for i in range(0,17):
        points[i] = points2[i]
    triangles = get_triangles(points)
    for i in triangles:
    #     print(i)
        
        # Calculate the frame of triangles
        x = i[0]
        y = i[1]
        z = i[2]

        tri1 = [points1[x], points1[y], points1[z]]
        tri2 = [points2[x], points2[y], points2[z]]
        tri = [points[x], points[y], points[z]]
    #     print(tri1, tri2, tri)
    #     print('-----')
        rect1 = cv2.boundingRect(np.float32([tri1]))
        rect2 = cv2.boundingRect(np.float32([tri2]))
        rect = cv2.boundingRect(np.float32([tri]))

        tri_rect1 = []
        tri_rect2 = []
        tri_rect_warped = []
        
        for i in range(0, 3):
            tri_rect_warped.append(((tri[i][0] - rect[0]), (tri[i][1] - rect[1])))
            tri_rect1.append(((tri1[i][0] - rect1[0]), (tri1[i][1] - rect1[1])))
            tri_rect2.append(((tri2[i][0] - rect2[0]), (tri2[i][1] - rect2[1])))
        
        # Accomplish the affine transform in triangles
        img1_rect = img1[rect1[1]:rect1[1] + rect1[3], rect1[0]:rect1[0] + rect1[2]]
        img2_rect = img2[rect2[1]:rect2[1] + rect2[3], rect2[0]:rect2[0] + rect2[2]]

        size = (rect[2], rect[3])
        warped_img1 = affine_transform(img1_rect, tri_rect1, tri_rect_warped, size)
        warped_img2 = affine_transform(img2_rect, tri_rect2, tri_rect_warped, size)
        
        # Calculate the result based on alpha
        extra = [67,68,69,70,71,72,73,74,75]
        jaw = list(range(0,17))
        if x in extra or y in extra or z in extra:
            alpha = 0
        else:
            alpha = 0
        img_rect = (1.0 - alpha) * warped_img1 + alpha * warped_img2

        # Generate the mask
        mask = np.zeros((rect[3], rect[2], 3), dtype=np.float32)
        cv2.fillConvexPoly(mask, np.int32(tri_rect_warped), (1.0, 1.0, 1.0), 16, 0)

        img_morphed[rect[1]:rect[1] + rect[3], rect[0]:rect[0] + rect[2]] = \
            img_morphed[rect[1]:rect[1] + rect[3], rect[0]:rect[0] +
                rect[2]] * (1 - mask) + img_rect * mask

    return img_morphed

def action_reshaped(source_img_name, target_img_name):
    Xs_raw = cv2.imread(source_img_name)
    # print(type(Xs_raw))
    try:
        Xs = detector.align(Image.fromarray(Xs_raw[:, :, ::-1]), crop_size=(256, 256))
        Xs_reserved = np.asarray(Xs)
    except Exception as e:
        print('the source image is wrong, please change the image')
    Xs_raw = np.array(Xs)[:, :, ::-1]
    Xs = test_transform(Xs)
    Xs = Xs.unsqueeze(0).cuda()

    with torch.no_grad():
        embeds = arcface(F.interpolate(Xs[:, :, 19:237, 19:237], (112, 112), mode='bilinear', align_corners=True))

    Xt_raw = cv2.imread(target_img_name)
    ## new reshape algorithm
    Xt_raw = transform(Xt_raw, Xs_reserved)
    # cv2.imwrite('test_reshape.jpg', Xt_raw)

    try:
        Xt, trans_inv = detector.align(Image.fromarray(Xt_raw[:, :, ::-1]), crop_size=(256, 256), return_trans_inv=True)
        # Xt_reserved = np.asarray(Xt)
    except Exception as e:
        print('the target image is wrong, please change the image')
    
    ## old reshape
    # Xt_reshaped = reshape(Xs_reserved, Xt_reserved)
    # Xt = Image.fromarray(Xt_reshaped)
    Xt_raw = Xt_raw.astype(np.float)/255.0
    Xt = test_transform(Xt)
    Xt = Xt.unsqueeze(0).cuda()

    mask = np.zeros([256, 256], dtype=np.float)
    for i in range(256):
        for j in range(256):
            dist = np.sqrt(((i-128)**2 + (j-128)**2))/128
            dist = np.minimum(dist,1)
            mask[i, j] = 1-1*dist

    mask = cv2.dilate(mask, None, iterations=20)

    with torch.no_grad():
        Yt, _ = G(Xt, embeds)
        Yt = Yt.squeeze().detach().cpu().numpy().transpose([1, 2, 0])*0.5+0.5
        Yt = Yt[:, :, ::-1]
        Yt_trans_inv = cv2.warpAffine(Yt, trans_inv, (np.size(Xt_raw, 1), np.size(Xt_raw, 0)), borderValue=(0, 0, 0))
        mask_ = cv2.warpAffine(mask,trans_inv, (np.size(Xt_raw, 1), np.size(Xt_raw, 0)), borderValue=(0, 0, 0))
        mask_ = np.expand_dims(mask_, 2)
        # print(type(Yt))
        # show(Yt*255)
        Yt_trans_inv = mask_*Yt_trans_inv + (1-mask_)*Xt_raw
        # print(Yt_trans_inv*255)
        return Yt_trans_inv*255

def action(source_img_name, target_img_name):
    Xs_raw = cv2.imread(source_img_name)
    # print(type(Xs_raw))
    try:
        Xs = detector.align(Image.fromarray(Xs_raw[:, :, ::-1]), crop_size=(256, 256))
    except Exception as e:
        print('the source image is wrong, please change the image')
    Xs_raw = np.array(Xs)[:, :, ::-1]
    Xs = test_transform(Xs)
    Xs = Xs.unsqueeze(0).cuda()

    with torch.no_grad():
        embeds = arcface(F.interpolate(Xs[:, :, 19:237, 19:237], (112, 112), mode='bilinear', align_corners=True))

    Xt_raw = cv2.imread(target_img_name)
    try:
        Xt, trans_inv = detector.align(Image.fromarray(Xt_raw[:, :, ::-1]), crop_size=(256, 256), return_trans_inv=True)
    except Exception as e:
        print('the target image is wrong, please change the image')
    Xt_raw = Xt_raw.astype(np.float)/255.0
    Xt = test_transform(Xt)
    Xt = Xt.unsqueeze(0).cuda()

    mask = np.zeros([256, 256], dtype=np.float)
    for i in range(256):
        for j in range(256):
            dist = np.sqrt(((i-128)**2 + (j-128)**2))/128
            dist = np.minimum(dist,1)
            mask[i, j] = 1-1*dist

    mask = cv2.dilate(mask, None, iterations=20)

    with torch.no_grad():
        Yt, _ = G(Xt, embeds)
        Yt = Yt.squeeze().detach().cpu().numpy().transpose([1, 2, 0])*0.5+0.5
        Yt = Yt[:, :, ::-1]
        Yt_trans_inv = cv2.warpAffine(Yt, trans_inv, (np.size(Xt_raw, 1), np.size(Xt_raw, 0)), borderValue=(0, 0, 0))
        mask_ = cv2.warpAffine(mask,trans_inv, (np.size(Xt_raw, 1), np.size(Xt_raw, 0)), borderValue=(0, 0, 0))
        mask_ = np.expand_dims(mask_, 2)
        # print(type(Yt))
        # show(Yt*255)
        Yt_trans_inv = mask_*Yt_trans_inv + (1-mask_)*Xt_raw
        # print(Yt_trans_inv*255)
        return Yt_trans_inv*255


from flask import Flask, jsonify, request, session
from datetime import timedelta
from flask_cors import CORS
import os

swap_server = Flask(__name__)
CORS(swap_server)

from PIL import Image, ImageDraw, ImageFont
import math
from config import (
    SECRET_KEY,
    WATERMARK_NORMAL,
    WATERMARK_STRESS,
    BASE_PATH
)

swap_server.secret_key = SECRET_KEY

swap_server.config['PERMANENT_SESSION_LIFETIME'] =  timedelta(minutes=5)



def watermark(img_path):
    target = Image.open(img_path) 
    width, height = target.size

    text_to_be_rotated = WATERMARK_NORMAL
    #text_to_be_rotated = ''
    message_length = len(text_to_be_rotated)

    # load font (tweak ratio based on your particular font)
    FONT_RATIO = 2.2
    DIAGONAL_PERCENTAGE = .1
    diagonal_length = int(math.sqrt((width**2) + (height**2)))
    diagonal_to_use = diagonal_length * DIAGONAL_PERCENTAGE
    # font_size = int(diagonal_to_use / (message_length / FONT_RATIO))
    font_size = (width + height)// 50
    font = ImageFont.truetype('arial.ttf', font_size)
    # font = ImageFont.load_default() # fallback

    margin = 10
    opacity = 200
    angle = math.degrees(math.atan(height/width))

    # watermark
    mark_width, mark_height = font.getsize(text_to_be_rotated)
    watermark = Image.new('RGBA', (mark_width, mark_height), (0, 0, 0, 0))
    # watermark.show()
    draw = ImageDraw.Draw(watermark)
    draw.text((0, 0), text=text_to_be_rotated, font=font, fill=(255, 255, 255, opacity))
    watermark = watermark.rotate(angle, expand=1)
    wx, wy = watermark.size
    px = 0
    py = 0
    # px = int((width - wx)/2)
    # py = int((height - wy)/2)
    target.paste(watermark, (px, py, px + wx, py + wy), watermark)

    px = width - wx - margin
    py = height - wy - margin
    target.paste(watermark, (px, py, px + wx, py + wy), watermark)

    out = np.asarray(target)
    out = cv2.cvtColor(out, cv2.COLOR_BGR2RGB)
    return out

session_json = {}
@swap_server.route('/api/swapping/', methods = ["POST"])
def api():
    global session_json
    if request.method == "POST":
        path_sys = BASE_PATH
        src_img = request.values["src_img"]
        reshape = request.json.get("reshape", False)
        src_path = os.path.join(path_sys, src_img)
        folder_name = request.values["folder_name"]

        folder_path = os.path.join(path_sys, folder_name)
        # print("nayla gi",src_path,folder_path)
        list_dst_imgs = os.listdir(folder_path)
        
        output_return = []
        output_to_download = []
        for i in list_dst_imgs:
            x = os.path.join(folder_path, i)
            ori = x.split("/web")[-1]
            try:
                if reshape:
                    img = action_reshaped(src_path, x)
                else:
                    img = action(src_path, x)
            except Exception as e:
                warning(e)
                continue
            try:
                if not os.path.exists(f'{path_sys}/static/swapped/{folder_name.split("/")[-1]}'):
                    os.mkdir(f'{path_sys}/static/swapped/{folder_name.split("/")[-1]}')
                if not os.path.exists(f'{path_sys}/swapped/{folder_name.split("/")[-1]}'):
                    os.mkdir(f'{path_sys}/swapped/{folder_name.split("/")[-1]}')
            except:
                pass
            name_save = f'{path_sys}/swapped/{folder_name.split("/")[-1]}/{src_img.split("/")[-1]}_{i}.jpg'

            name_save_watermarked = f'{path_sys}/static/swapped/{folder_name.split("/")[-1]}/{src_img.split("/")[-1]}_{i}.jpg'
            cv2.imwrite(name_save, img)
            out = watermark(name_save)
            cv2.imwrite(name_save_watermarked, out)
            output_to_download.append(name_save)
            output_return.append([f'/static{name_save_watermarked.split("static")[-1]}', ori])

        # save folder download
        # session.permanent = True
        folder_name = folder_name.split("/")[-1]
        session_json.update({folder_name: output_to_download})
        
        return jsonify(data = {
            'imgs': output_return
            })


@swap_server.route('/api/crop-face/', methods = ["POST"])
def api_cropface():
    if request.method == "POST":
        img_path = BASE_PATH + request.values["img_path"]
        crop_face(img_path)
        return jsonify(data=True)

@swap_server.route('/api/via-email/', methods=["POST"])
def api_via_email():
    global session_json
    if request.method == "POST":
        # print(session["3123123"])
        if "folder_name" in request.values:
            folder_name = request.values["folder_name"]
            folder_name = folder_name.split("/")[-1]
        # session.permanent = True
        if folder_name in session_json:
            print(session_json[folder_name])
            return jsonify(data={
                "imgs": session_json[folder_name],
            })
        return jsonify(data={
                "imgs": 1,
            })

if __name__ == "__main__":
    swap_server.run(host = "0.0.0.0", port=8081, debug=1)
            
